package aws.user.rest;

import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Component
@FeignClient(name = "aws-login-rest", path = "/aws-login-rest")
@RibbonClient(name = "aws-login-rest")
public interface LoginClient { 
	
	@GetMapping(value = "/login")
	public String login(@RequestParam String msg);
	
	@GetMapping(value = "/go")
	public String go();
	
}
