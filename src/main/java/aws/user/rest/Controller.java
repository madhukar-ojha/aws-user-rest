package aws.user.rest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {
	
	private static final Logger logger = LogManager.getLogger(Controller.class);
	
	@Autowired LoginClient loginClient; 
	@GetMapping(value = "/login/{msg}")
	public String login(@PathVariable String msg) {
		logger.info("Controller :: login :: start");
		String res = loginClient.login(msg);
		logger.info("Controller :: login :: end");
		return res;
	}
	
	@GetMapping(value = "/go")
	public String go() {
		logger.info("Controller :: go :: start");
		String res = loginClient.go();
		logger.info("Controller :: go :: end");
		return res;
	}
}
