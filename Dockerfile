FROM openjdk:8
ADD target/aws-user-rest.jar aws-user-rest.jar
COPY ./target/aws-user-rest.jar .
WORKDIR .
ARG JAR_FILE
RUN sh -c 'touch aws-user-rest.jar'
EXPOSE 8071
ENTRYPOINT ["java", "-jar", "aws-user-rest.jar"]
